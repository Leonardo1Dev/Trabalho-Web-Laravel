<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class Semeador extends Seeder
{
    // Gera um nome e um email aleatórios
    // Ambos nome e email são relacionados
    private function gerarNomeAleatorio() {
        $primeirosNomes = [
            'João', 'Maria', 'Pedro', 'Ana', 'José', 'Mariana', 'Carlos', 'Lúcia', 'Fernando', 'Isabela',
            'Antônio', 'Luiza', 'Paulo', 'Amanda', 'Rafael', 'Camila', 'Ricardo', 'Patrícia', 'Daniel','Laura', 'Miguel', 'Gabriela', 'Eduardo', 'Beatriz', 'Lucas', 'Letícia', 'Gustavo', 'Natália', 'Matheus', 'Juliana', 'Rodrigo', 'Carolina', 'André', 'Bruna', 'Raul', 'Tatiana', 'Fábio', 'Manuela', 'Roberto', 'Cristina', 'Renato', 'Valentina', 'Hugo', 'Daniela', 'Alexandre', 'Raquel', 'Marcelo', 'Bianca', 'Guilherme', 'Isadora'
        ];

        $sobrenomes = [
            'Silva', 'Santos', 'Pereira', 'Lima', 'Ribeiro', 'Almeida', 'Gomes', 'Oliveira', 'Rodrigues', 'Ferreira', 'Martins', 'Carvalho', 'Melo', 'Barbosa', 'Rocha', 'Reis', 'Moreira', 'Mendes', 'Nascimento', 'Monteiro', 'Coelho', 'Castro', 'Freitas', 'Cunha', 'Costa', 'Cavalcanti', 'Batista', 'Sousa', 'Gonçalves', 'Pinto', 'Teixeira', 'Machado', 'Cardoso', 'Faria', 'Vieira', 'Dias', 'Araújo', 'Fernandes', 'Campos', 'Tavares', 'Pinheiro', 'Andrade', 'Correia', 'Azevedo', 'Nunes', 'Cruz', 'Medeiros', 'Peixoto', 'Moraes', 'Leite'
        ];
    
        $nome = $primeirosNomes[array_rand($primeirosNomes)];
        $sobrenome1 = $sobrenomes[array_rand($sobrenomes)];
        $sobrenome2 = $sobrenomes[array_rand($sobrenomes)];
        $sobrenome3 = $sobrenomes[array_rand($sobrenomes)];
        
        $email = 
            strtolower(str_replace(' ', '', $nome)) . '.' . 
            strtolower(str_replace(' ', '', $sobrenome1)) . '.' .
            strtolower(str_replace(' ', '', $sobrenome2)) . '.' .
            strtolower(str_replace(' ', '', $sobrenome3)) .
            '@example.com';

        return [
            'nome' => $nome . ' ' . $sobrenome1 . ' ' . $sobrenome2 . ' ' . $sobrenome3,
            'email' => $email,
        ];
    }

    // Gerar uma matricula de 10 dígitos
    private function gerarMatricula() {
        $numero = '';
        for ($i = 0; $i < 10; $i++) {
            $digito = mt_rand(0, 9);
            $numero .= $digito;
        }
        return $numero;
    }

    // Gera um aluno
    private function geraAluno(): void
    {
        $falsificador = $this->gerarNomeAleatorio();

        $aluno = new User();
        $aluno->nome = $falsificador['nome'];
        $aluno->email = $falsificador['email'];
        $aluno->password = bcrypt('1234');
        $aluno->matricula = $this->gerarMatricula();
        $aluno->aluno = 1;
        $aluno->confirmado = mt_rand(0, 1);
        $aluno->save();
    }

    // Gera um funcionario
    private function geraFuncionario(): void
    {
        $falsificador = $this->gerarNomeAleatorio();

        $aluno = new User();
        $aluno->nome = $falsificador['nome'];
        $aluno->email = $falsificador['email'];
        $aluno->password = bcrypt('1234');
        $aluno->matricula = $this->gerarMatricula();
        $aluno->funcionario = 1;
        $aluno->confirmado = mt_rand(0, 1);
        $aluno->save();
    }

    public function run(): void
    {
        for ($i=0; $i < 30; $i++) {
            $this->geraAluno();
        }

        for ($i=0; $i < 10; $i++) {
            $this->geraFuncionario();
        }
    }
}
