<?php

use App\Models\Ocorrencia;
use GuzzleHttp\Promise\Create;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OcorrenciaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\RotasController::class, 'rota_raiz'])->name('home');

Route::get('/cad_user', [App\Http\Controllers\RotasController::class, 'rota_cad_user'])->name('cad');

Route::post('/cad_users_back', [App\Http\Controllers\UserController::class, 'cad_users'])->name('cadastra_users_back_end');
Route::middleware('auth', 'func_admin')->group(function () {

    Route::get('/ocorrencia/cadastro', [App\Http\Controllers\OcorrenciaController::class, 'create'])->name('ocorrencia.create');

    Route::post('/ocorrencia/cadastro', [App\Http\Controllers\OcorrenciaController::class, 'store'])->name('ocorrencia.store');

    Route::get('/lista-ocorrencias', [App\Http\Controllers\OcorrenciaController::class,'read'])->name('ocorrencia.lista');

});
Route::get('/logout', [App\Http\Controllers\UserController::class, 'logout'])->name('logout');

Route::middleware('auth', 'adminstrador')->group(function () {
    Route::get('/confirmar/{id}', [App\Http\Controllers\UserController::class, 'confirmar'])->name('confirmar');

    Route::get('/editar/{id}', [App\Http\Controllers\OcorrenciaController::class, 'editar'])->name('ocorrencia.editar');

    Route::get('/editar/{id}', [App\Http\Controllers\OcorrenciaController::class, 'editar2'])->name('ocorrencia.editar2');

    Route::get('/lista-usuarios', [App\Http\Controllers\UserController::class, 'read'])->name('usuario.lista');
    Route::get('/lista-ocorrencias', [App\Http\Controllers\OcorrenciaController::class, 'search'])->name('buscar.aluno');

});

Route::post('/login', [App\Http\Controllers\UserController::class, 'login'])->name('login');

Route::get('/lista-ocorrencias/{id}', [App\Http\Controllers\OcorrenciaController::class, 'searchBy'])->name('ocorrencias.minha');