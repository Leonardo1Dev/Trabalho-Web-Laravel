@extends('template.header')

@section('ocorrencias')
    <div class="container">

        <br>
        <div>
            {{ $dados->links() }}
        </div>
        <br>
            <table class="table table-dark rounded">
                <thead>
                    <tr>
                        <th scope="col">Titulo</th>
                        <th scope="col">Gravidade</th>
                        <th scope="col">Aluno envolvido</th>
                        <th scope="col">Descrição</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dados as $un)
                        <tr>
                            <th scope="row">{{ $un->titulo }}</th>

                            <td>
                                @if ($un->gravidade == 1)
                                    Leve
                                @elseif($un->gravidade == 2)
                                    Média
                                @else
                                    Alta
                                @endif
                            </td>
                            <td>{{ $un->nome }}</td>
                            <td>
                                <button type="button" class="btn btn-dark" data-toggle="modal"
                                    data-target="#exampleModal{{ $un->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                        <path
                                            d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                        <path
                                            d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                    </svg>
                                </button>
                            </td>
                        </tr>


                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal{{ $un->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $un->descricao }}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </tbody>
            </table>
    </div>
@endsection
