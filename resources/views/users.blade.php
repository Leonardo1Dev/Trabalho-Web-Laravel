@extends('template.header')

@section('users')
    <div class="container">
        <br>
        @if (auth()->check() && Auth::user()->admin === 1)
            <div>
                {{ $dados->links() }}
            </div>
            <br>
            <table class="table table-dark rounded">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Matrícula</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dados as $d)
                        <tr>
                            <th scope="row">{{ $d->nome }}</th>
                            <td>{{ $d->email }}</td>
                            <td>{{ $d->matricula }}</td>
                            @if ($d->confirmado == 0)
                                <td><a class="btn btn-dark" href="{{route('confirmar', ['id' => $d->id])}}">Confirmar</a></td>
                            @else
                                <td>Confirmado</td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection