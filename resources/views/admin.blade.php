@extends('template.header')

@section('admin')
    <div class="container">
        
        <br>
        @if (auth()->check() && Auth::user()->admin === 1)
            <div>
                {{ $unconfirmeds->links() }}
            </div>
            <br>
            <table class="table table-dark rounded">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Matrícula</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($unconfirmeds as $un)
                        <tr>
                            <th scope="row">{{ $un->nome }}</th>
                            <td>{{ $un->email }}</td>
                            <td>{{ $un->matricula }}</td>
                            <td>
                                <a class="btn btn-dark" href="{{route('confirmar',['id' => $un->id])}}">Confirmar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
