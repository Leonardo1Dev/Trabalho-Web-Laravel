<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/header.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <title> Controle de Ocorrências</title>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ route('home') }}"><img src="https://img.icons8.com/?size=512&id=84005&format=png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado"
            aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
            @if (auth()->check())
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @if (Auth::user()->admin === 1 || Auth::user()->funcionario === 1)
                                <a class="dropdown-item" href="{{ route('ocorrencia.create') }}">Cadastrar Ocorrencia</a>
                            @endif
                            @if (Auth::user()->admin === 1)
                                <a class="dropdown-item" href="{{ route('usuario.lista') }}">Lista de Usuários</a>
                                <a class="dropdown-item" href="{{ route('buscar.aluno') }}">Lista de Ocorrências</a>
                            @endif
                            @if (Auth::user()->aluno === 1)
                                <a class="dropdown-item" href="{{ route('ocorrencias.minha', ['id' => Auth::user()->id]) }}">Lista de Ocorrências</a>
                            @endif
                        </div>
                    </li>
                    @if (auth()->check())
                        <li class="nav-item">
                            <a class="btn btn-danger" href="{{ route('logout') }}"> Sair </a>
                    @endif
            @endif

            @if (auth()->check() && (Auth::user()->admin === 1 || Auth::user()->funcionario === 1))
                </ul>
                <form action="{{ route('buscar.aluno')}}" method="GET" class="form-inline my-2 my-lg-0">
                    <input name="inputNome" class="form-control mr-sm-2" type="search" placeholder="Buscar por aluno" aria-label="Pesquisar">
                    <button class="btn btn-outline-dark" type="submit"><i class="bi bi-search"><svg
                                xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-search" viewBox="0 0 16 16">
                                <path
                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                            </svg></i>
                    </button>
                </form>
            @endif
        </div>
    </nav>

    @include('sweetalert::alert')
    @yield('login-cad')
    @yield('cad')
    @yield('admin')
    @yield('cadastro')
    @yield('ocorrencias')
    @yield('users')
    @yield('myocor')
</body>
</html>