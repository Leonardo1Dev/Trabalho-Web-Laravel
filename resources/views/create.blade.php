@extends('template.header')

@section('cadastro')
    <br>
    <div class="body-background">
        <div class="container-fluid d-flex justify-content-center align-items-center h-100">

            <div class="card p-3 text-center py-4">
                <h4>Cadastrar ocorrencia</h4>



                <form method="post" action="{{ route('ocorrencia.store') }}">
                    @csrf
                    <div class="input-group px-3 mt-3">
                        <input type="text" name="titulo" class="form-control" placeholder="Titulo" aria-label="Username">
                        <span></span>
                    </div>
                    <div class="mt-3 px-3">

                        <select name="infrator_id" class="custom-select" id="inputGroupSelect01">
                            <option selected>Infrator</option>
                            @foreach ($infrator as $us)
                                <option value="{{ $us->id }}"> {{ $us->nome }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="mt-3 px-3">
                        <select name="delator_id" class="custom-select" id="inputGroupSelect01">
                            <option selected>Delator</option>
                            @foreach ($delator as $us1)
                                <option value="{{ $us1->id }}"> {{ $us1->nome }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="mt-3 px-3">
                        <textarea name="descricao" class="form-control" placeholder="Descricao" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div class="mt-3 px-3">
                        <select name="gravidade" class="custom-select" id="inputGroupSelect01">
                            <option selected>Gravidade da Infração</option>

                            <option value="1"> Leve</option>
                            <option value="2"> Média</option>
                            <option value="3"> Alta</option>


                        </select>
                    </div>
                    <div class="mt-3 d-grid px-3">
                        <button class="btn btn-primary btn-block btn-signup text-uppercase">
                            <span>Cadastrar</span>

                        </button>
                    </div>


            </div>

        </div>
    </div>
@endsection
