<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class RotasController extends Controller
{
    function rota_raiz()
    {
        if (Auth::user() && (Auth::user()->admin === 1 || Auth::user()->funcionario ===1)) {
            $unconfirmeds = User::where('confirmado', '<>', '1')->simplePaginate(10);
            return view('admin', compact('unconfirmeds'));
        }elseif(Auth::user() && Auth::user()->aluno ===1){
            return redirect()->route('ocorrencias.minha', ['id' => Auth::user()->id]);
        } else {
            return view('login-cad');
        }
    }
    function rota_cad_user()
    {
        if (Auth::user()) {

        } else {

            return view('cad');
        }

    }
}