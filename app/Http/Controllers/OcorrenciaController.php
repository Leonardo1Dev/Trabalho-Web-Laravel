<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Ocorrencia;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class OcorrenciaController extends Controller
{
    public function create()
    {
        $infrator = User::where('aluno', 1)->get();
        $delator = User::where('aluno', 1)->get();
        return view('create', compact('infrator', 'delator'));
    }

    public function store(Request $request)
    {
        // Valide os dados do formulário
        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'gravidade' => 'required',
            'infrator_id' => 'required',
            'delator_id' => 'required',
            // Adicione aqui as outras regras de validação necessárias
        ]);

        // Crie uma nova ocorrência
        $ocorrencia = new Ocorrencia($request->only('titulo', 'infrator_id', 'descricao', 'delator_id', 'gravidade'));
        $ocorrencia->funcionario_id = Auth::user()->id;

        if ($ocorrencia->save()) {
            Alert::success('Aviso!', 'A ocorrencia foi cadastrada');
        } else {
            Alert::error('Ops!', 'Houve um erro durante o cadastro');
        }
        return redirect()->route('ocorrencia.store');
    }

    public function read()
    {
        $dados = DB::table('ocorrencias')
            ->join('users as ui', 'ui.id', '=', 'ocorrencias.infrator_id')
            ->select('ocorrencias.*', 'ui.nome')
            ->simplePaginate(20);

        return view('ocorrencias', compact('dados'));
    }

    public function search(Request $request)
    {
        $searchItem = $request->input('inputNome');

        $dados = DB::table('ocorrencias')
            ->join('users as ui', 'ui.id', '=', 'ocorrencias.infrator_id')
            ->select('ocorrencias.*', 'ui.nome')
            ->where('ui.nome', 'LIKE', "%{$searchItem}%")
            ->orWhere('ui.nome', 'LIKE', "%{$searchItem}%")
            ->simplePaginate(20);

        return view('ocorrencias', compact('dados'));
    }

    public function searchBy($id)
    {
        $dados = Ocorrencia::where('infrator_id','=',$id)->simplePaginate(20);
        return view('myocor', compact('dados'));
    }

}