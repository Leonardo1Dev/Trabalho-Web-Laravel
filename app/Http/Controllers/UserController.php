<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    function cad_users(Request $request)
    {
        $request->validate([
            'nome' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
            'matricula' => ['required'],
        ], [
            'nome.required' => 'O nome é obrigatório',
            'email.required' => 'O Email é obrigatório',
            'password.required' => 'A Senha é obrigatória',
            'matricula.required' => 'A Matricula é obrigatória',
        ]);


        $new_user = new User($request->only('nome', 'email', 'password', 'matricula'));
        $new_user->confirmado = 0;
        if ($request->decisao === 'aluno') {
            $new_user->aluno = 1;
        } elseif ($request->decisao === 'funcionario') {
            $new_user->funcionario = 1;
        }
        if ($new_user->save()) {
            Alert::success("Tudo certo!", "O usuário $new_user->nome foi cadastrado, mas ainda esta sujeito a confirmação de dados!");
            return redirect()->route('home');

        }
    }

    function login(Request $request)
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ], [
            'email.required' => 'O Email é obrigatório',
            'password.required' => 'A Senha é obrigatória',
        ]);
        $credenciais = $request->only('email', 'password');
        if (Auth::attempt($credenciais) && Auth::user()->confirmado === 1) {
            $request->session()->regenerate();
            Alert::success("Tudo certo!", "Bem Vindo " . Auth::user()->nome);
        } else if (Auth::check()) {
            Auth::logout();
            Alert::error('Ops', "Você não esta confirmado ainda!");
        } else {
            Alert::error("Ops!", "Erro ao realizar login!");
        }


        return redirect()->route('home');
    }

    function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    function confirmar($id)
    {
        $usuario = User::find($id);
        $usuario->confirmado = 1;
        if ($usuario->save()) {
            Alert::success("Tudo certo!", "Usuário confirmado!");
        } else {
            Alert::error("Ops", "Houve algum erro!");
        }
        return redirect()->route('home');
    }

    function read()
    {
        $dados = DB::table('users')->select('users.*')->simplePaginate(10);
        return view('users', compact('dados'));
    }
}